const express = require('express');
const router = express.Router();
const verifyJWT = require('../middlewares/verifyToken');

const {getUser, deleteUser, updateUser} = require('../controllers/manageUser')

// get current user
router.get('/me', verifyJWT, getUser);

// delete current user
router.delete('/me', verifyJWT, deleteUser)

// change password
router.patch('/me', verifyJWT, updateUser)

module.exports = router;