import UserAuth from "../userAuth/UserAuth";
import QueryResult from "../queryResult/QueryResult";
import {useState} from 'react';
import UserActions from '../userActions/UserActions';
import NotesCreation from "../notesCreation/NotesCreation";

const App = () => {

  const [response, setResponse] = useState('');
  const [authToken, setAuthToken] = useState('');

  const getQueryResult = (result, token) => {
    setResponse(result);
    setAuthToken(token);
  }

  return (
    <div className="wrapper">
      <div className="inputContent content">
        <UserAuth getQueryResult={getQueryResult}/>
        <UserActions getQueryResult={getQueryResult} authToken={authToken}/>
        <NotesCreation getQueryResult={getQueryResult} authToken={authToken}/>
      </div>
      <div className="outputContent content">
        <QueryResult result={response}/>
      </div>
    </div>
  )
}

export default App;
