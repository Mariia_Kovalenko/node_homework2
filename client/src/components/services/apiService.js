import axios from 'axios';

class ApiService {
    async registerUser(url, params) {
        // console.log('register');
        try {
            let response = axios.post(url + 'auth/register', {
                username: params.username,
                password: params.password
            })
            return response
        } catch (error) {
            console.log(error);
        }
    }

    async loginUser(url, params) {
        // console.log('login');
        try {
            let response = axios.post(url + 'auth/login', {
                username: params.username,
                password: params.password
            })
            return response
        } catch (error) {
            console.log(error.message);
        }
    }

    async getUserInfo(url, token) {
        // console.log('authorizing with token', token);
        try {
            let response = axios.get(url + 'users/me', {
                headers: {
                    "authorization": `Bearer ${token}`
                }
            })
            return response
        } catch (error) {
            console.log(error.message);
        }
    }

    async deleteUser(url, token) {
        // console.log('authorizing with token', token);
        try {
            let response = axios.delete(url + 'users/me', {
                headers: {
                    "authorization": `Bearer ${token}`
                }
            })
            return response
        } catch (error) {
            console.log(error.message);
        }
    }

    async changePassword(url, params, token) {
        // console.log('change password');
        try {
            let response = axios.patch(url + 'users/me', {
                oldPassword: params.oldPassword,
                newPassword: params.newPassword
            }, {
                headers: {
                    "authorization": `Bearer ${token}`
                }
            });
            return response
        } catch (error) {
            console.log(error);
        }
    }

    async getUserNotes(url, token) {
        // console.log('get notes');
        try {
            let response = axios.get(url + 'notes', {
                headers: {
                    "authorization": `Bearer ${token}`
                }
            })
            return response
        } catch (error) {
            console.log(error.message);
        }
    }

    async createUserNote(url, params, token) {
        // console.log('create note');
        try {
            let response = axios.post(url + 'notes', 
            {
                text: params.text
            },
            {
                headers: {
                    "authorization": `Bearer ${token}`
                }
            })
            return response
        } catch (error) {
            console.log(error.message);
        }
    }

    async findNoteById(url, id, token) {
        // console.log('get note');
        try {
            let response = axios.get(url + 'notes/' + id, {
                headers: {
                    "authorization": `Bearer ${token}`
                }
            })
            return response
        } catch (error) {
            console.log(error.message);
        }
    }
    async deleteNoteById(url, id, token) {
        // console.log('delete note');
        try {
            let response = axios.delete(url + 'notes/' + id, {
                headers: {
                    "authorization": `Bearer ${token}`
                }
            })
            return response
        } catch (error) {
            console.log(error.message);
        }
    }
    
    async markNote(url, id, token) {
        // console.log('check note');
        try {
            axios.defaults.headers.authorization = `Bearer ${token}`;
            let response = axios.patch(url + 'notes/' + id)
            return response
        } catch (error) {
            console.log(error.message);
        }
    }

    async updateNote(url, id, body, token) {
        try {
            // axios.defaults.headers.authorization = `Bearer ${token}`;
            let response = axios.put(url + 'notes/' + id, {
                text: body.text
            },
            {
                headers: {
                    "authorization": `Bearer ${token}`
                }
            }
            )
            return response
        } catch (error) {
            console.log(error.message);
        }
    }
}

export default ApiService;