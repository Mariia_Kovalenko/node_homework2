import {useState} from 'react';
import {BASE_URL} from '../services/_constants';
import ApiService from '../services/apiService';

const NotesCreation = (props) => {
    let authToken = props.authToken;

    if (!authToken ) {
        authToken = localStorage.getItem("token");
    }

    const disabled = !authToken || authToken === 'undefined' ? true : false;
    const apiService = new ApiService();

    const [noteId, setNoteId] = useState('');
    const [noteText, setNoteText] = useState('');
    const [newText, setNewText] = useState('');

    const getQueryResult = (resp) => {
        props.getQueryResult(resp);
    }

    const getUserNotes = () => {
        getQueryResult('Loading...')

        apiService.getUserNotes(BASE_URL, authToken)
            .then(res => {
                console.log(res);

                if (!res.data.notes.length) {
                    getQueryResult('You have no notes yet');
                    return;
                } 

                const notesList = res.data.notes.map(note => {
                    return {
                        id: note._id,
                        text: note.text,
                        completed: note.completed
                    }
                });
                getQueryResult(notesList);
            }).catch(error => {
                // console.log(error);
                getQueryResult('Something went wrong');
            })
    }

    const createUserNote = () => {
        if (!noteText) {
            getQueryResult('Enter note text!');
            return;
        }

        getQueryResult('Loading...');

        apiService.createUserNote(BASE_URL, { text: noteText }, authToken)
            .then(res => {
                // console.log(res);
                getQueryResult(res.data.message);
                setNoteText('');
            }).catch(error => {
                // console.log(error);
                getQueryResult('Failed creating note')
                setNoteText('');
            })
    }

    const findNoteById = () => {
        if (!noteId) {
            getQueryResult('Enter note id!');
            return;
        }
        
        getQueryResult('Loading...');

        apiService.findNoteById(BASE_URL, noteId, authToken)
            .then(res => {
                // console.log(res);
                const noteId = res.data.note._id;
                const created = res.data.note.createdDate;
                const text = res.data.note.text;
                const completed = res.data.note.completed;
                getQueryResult(`Note id: ${noteId}, created: ${created}, completed: ${completed}, text: ${text}`);
                setNoteId('');
            }).catch(error => {
                getQueryResult('No such note')
                setNoteId('');
            })
    }

    const deleteNoteById = () => {
        if (!noteId) {
            getQueryResult('Enter note id!');
            return;
        }
        
        getQueryResult('Loading...');

        apiService.deleteNoteById(BASE_URL, noteId, authToken)
            .then(res => {
                console.log(res);
                getQueryResult(`Success`);
                setNoteId('');
            }).catch(error => {
                getQueryResult('Something went wrong. Make sure you entered right data')
                setNoteId('');
            })
    }

    const markNote = () => {
        if (!noteId) {
            getQueryResult('Enter note id!');
            return;
        }

        getQueryResult('Loading...');

        apiService.markNote(BASE_URL, noteId, authToken)
            .then(res => {
                // console.log(res);
                getQueryResult(`Success`);
                setNoteId('');
            }).catch(error => {
                console.log(error);
                getQueryResult('Something went wrong. Make sure you entered right data')
                setNoteId('');
            })
    }

    const changeNoteText = () => {
        if (!noteId) {
            getQueryResult('Enter note id!');
            return;
        }
        if (!newText) {
            getQueryResult('Enter note text!');
            return;
        }

        getQueryResult('Loading...');
        
        apiService.updateNote(BASE_URL, noteId, {
            text: newText
        }, authToken)
            .then(res => {
                // console.log(res);
                getQueryResult(`Success`);
                setNoteId('');
                setNewText('');
            }).catch(error => {
                // console.log(error);
                getQueryResult('Something went wrong. Make sure you entered right data')
                setNoteId('');
                setNewText('');
            })
    }

    const getNoteText = (event) => {
        event.preventDefault();
        setNoteText(event.target.value);
    }
    const getNewText = (event) => {
        event.preventDefault();
        setNewText(event.target.value);
    }

    const getNoteId = (event) => {
        event.preventDefault();
        setNoteId(event.target.value);
    }

    return (
        <>
            <div>
                <div className='label'>Notes</div>
                <div className='user-actions-btns'>
                    <button  disabled={disabled} className='btn-submit' onClick={getUserNotes}>Get All</button>
                </div>
                <div className='label-tiny'>Create Note</div>
                <div className='note-inputs'>
                    <textarea
                    className='note-text'
                    required
                    placeholder='Enter note text'
                    rows="4"
                    value={noteText}
                    onChange={getNoteText}
                    />
                    <button disabled={disabled} 
                    className='btn-submit'
                    onClick={createUserNote}
                    >Add</button>
                </div>
                <div className='label-tiny'>Manage Note by id</div>
                <div className='form__inner'>
                    <input
                        className='user-input'
                        type="text"
                        placeholder='Note id'
                        value={noteId}
                        onChange={getNoteId}
                    />
                    <textarea
                    className='note-text'
                    required
                    placeholder='Enter new note text'
                    rows="4"
                    value={newText}
                    onChange={getNewText}
                    />
                </div>
                <div className='form__inner'>
                    <button  disabled={disabled} className='btn-submit action' onClick={findNoteById}>Find</button>
                    <button  disabled={disabled} className='btn-submit action-delete' onClick={deleteNoteById}>Delete</button>
                </div>
                <div className='form__inner'>
                    <button  disabled={disabled} className='btn-submit' onClick={changeNoteText}>Change Text</button>
                    <button  disabled={disabled} className='btn-submit' onClick={markNote}>Mark Note</button>
                </div>
            </div>
        </>
    )
}

export default NotesCreation;