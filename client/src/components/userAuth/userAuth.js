import {useState, useEffect} from 'react';
import {BASE_URL} from '../services/_constants';
import ApiService from '../services/apiService';

const UserAuth = (props) => {
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const apiService = new ApiService();

    const register = () => {
        // console.log('request to db');
        getQueryResult('Loading...')

        apiService.registerUser(BASE_URL, {
            username: name,
            password: password
        }).then(res => {
            if (res.status === 200) {
                getQueryResult(res.data.message);
            } 
        }).catch(error => {
            // console.log(error);
            getQueryResult(error.response.data);
        })
    }

    const login = () => {
        getQueryResult('Loading...')

        apiService.loginUser(BASE_URL, {
            username: name,
            password: password
        }).then(res => {
            // console.log('logged in');
            // console.log(res);
            if (res.status === 200) {
                getQueryResult(res.data.message, res.data.jwt_token);
            } 
        }).catch(error => {
            // console.log(error);
            getQueryResult(error.response.data.message || 'Access denied');
        })
    }

    const getQueryResult = (resp, token) => {
        localStorage.setItem("token", token);
        props.getQueryResult(resp, token);
        setName('');
        setPassword('');
    }

    const setUserName = (event) => {
        event.preventDefault();
        setName(event.target.value);
    }

    const setUserPass = (event) => {
        event.preventDefault();
        setPassword(event.target.value);
    }

    const onRegister = (e) => {
        e.preventDefault();
        if (name && password) {
            register();
        } else {
            getQueryResult('Enter Credentials!');
        }
    }

    const onLogin = (e) => {
        e.preventDefault();
        if (name && password) {
            login();
        } else {
            getQueryResult('Enter Credentials!');
        }
    }


    return (
        <div>
            <div className='label'>User Authentication</div>
            <form className='user-auth'>
                <div className='form__inner'>
                    <input
                    className='user-input'
                    type="text"
                    name="username"
                    placeholder="Name"
                    value={name}
                    onChange={setUserName}
                    />
                    <input
                    className='user-input'
                    type="password"
                    name="password"
                    placeholder="Password"
                    value={password}
                    onChange={setUserPass}
                    />
                </div>
                <div className='form__inner'>
                    <button className='btn-submit' type="button" onClick={onRegister}>Register</button>
                    <button className='btn-submit' type="button" onClick={onLogin}>Login</button>
                </div>
                
            </form>
        </div>
    );
}

export default UserAuth;