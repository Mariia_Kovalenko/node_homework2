import {useState, useEffect} from 'react';
import {BASE_URL} from '../services/_constants';
import ApiService from '../services/apiService';

const UserActions = (props) => {

    let authToken = props.authToken;
    // console.log(authToken);

    if (!authToken ) {
        authToken = localStorage.getItem("token");
        // console.log(authToken === 'undefined');
    }
    const disabled = !authToken || authToken === 'undefined' ? true : false;
    const apiService = new ApiService();

    const [oldPassword, setOldPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');

    const onSubmit = () => {
        changePassword(oldPassword, newPassword)
    }

    const setOldUserPassword = (event) => {
        event.preventDefault();
        setOldPassword(event.target.value);
    }

    const setNewUserPassword = (event) => {
        event.preventDefault();
        setNewPassword(event.target.value);
    }

    const getQueryResult = (resp) => {
        props.getQueryResult(resp);
        setOldPassword('');
        setNewPassword('');
    }

    const getUserInfo = () => {
        getQueryResult('Loading...');

        apiService.getUserInfo(BASE_URL, authToken)
            .then(res => {
                console.log('got info');
                if (res.status === 200) {
                    const userId = res.data.user._id;
                    const userName = res.data.user.username;
                    const created = res.data.user.createdDate;
                    getQueryResult(`User id:${userId}; \r\n name: ${userName}; \r\n created: ${created}`);
                }
            }).catch(error => {
                console.log(error);
                if (error.response.status === 400) {
                    getQueryResult('Please log in first')
                }
            })
    }

    const deleteUser = () => {
        getQueryResult('Loading...');

        apiService.deleteUser(BASE_URL, authToken)
            .then(res => {
                console.log('deleted');
                getQueryResult('User deleted');
            }).catch(error => {
                console.log(error);
                getQueryResult('Something went wrong')
            })
    }

    const changePassword = (oldPassword, newPassword) => {
        getQueryResult('Loading...');

        if (!(oldPassword && newPassword)) {
            getQueryResult('Enter credentials')
            setNewPassword('');
            setOldPassword('');
            return;
        }
        apiService.changePassword(BASE_URL, {
            oldPassword: oldPassword,
            newPassword: newPassword
        }, authToken)
            .then(res => {
                // console.log(res);
                getQueryResult('Password changed');
            }).catch(error => {
                getQueryResult('Something went wrong...');
            })
    }

    return (
        <div>
            <div className='label'>User Actions</div>
            <div className='user-actions-btns'>
                <button disabled={disabled} className='btn-submit action' onClick={getUserInfo}>Get Info</button>
                <button disabled={disabled} className='btn-submit action-delete' onClick={deleteUser}>Delete</button>
            </div>
            <div className='label-tiny'>Change Password</div>
            <form className='user-auth'>
                <div className='form__inner'>
                <input
                    className='user-input'
                    type="password"
                    name="oldPassword"
                    placeholder="Old Password"
                    value={oldPassword}
                    onChange={setOldUserPassword}/>
                    <input
                    className='user-input'
                    type="password"
                    name="newPassword"
                    placeholder="New Password"
                    value={newPassword}
                    onChange={setNewUserPassword}
                    />
                </div>
                <div className='form__inner'>
                    <button disabled={disabled} className='btn-submit' type="button" onClick={onSubmit}>Change Password</button>
                </div>
                
            </form>
        </div>
    )
}

export default UserActions;