
const QueryResult = (props) => {

    let result = props.result;
    let content;

    if (typeof result === 'object') {
        content = createList(result);
    } else {
        content = result
    }

    function createList(result) {
        return result.map(item => {
            return (
                <div key={item.id}>
                    <div>
                        Id: <span>{item.id}</span>
                    </div>
                    <div>
                        Text: <span>{item.text}</span>
                    </div>
                    <div>
                        Completed: <span>{String(item.completed)}</span>
                    </div>
                    <hr/>
                </div>
            )
        })
    }

    return (
        <div className="container">
            <div className="label">Result</div>
            <div className="message">
                {content}
            </div>
        </div>
    )
}

export default QueryResult