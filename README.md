# Description
This is a Node JS + React CRUD API for notes. 
## Run the application
To run the application, you need to run client and server sides separately. 

To launch the server run (in node_homework2 directory)
## `npm start`
The server will start on [http://localhost:8080](http://localhost:8080)

To run React application, move to client directory:
## `cd client`

Then, to start the app in development mode, run
## `npm start`

The app will be launched on [http://localhost:3000](http://localhost:3000)

